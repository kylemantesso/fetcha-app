export interface IMenuVariation {
  id: string;
  slug: string;
  name: string;
  price: number;
  image?: string;
  qty?: number;
}

export interface IMenuItem {
  id: string;
  slug: string;
  name: string;
  image: string;
  description: string;
  variations: IMenuVariation[];
}

export interface IMenuCategory {
  id: string;
  slug: string;
  name: string;
  items: IMenuItem[];
}

export interface IMenu {
  [key: string]: IMenuCategory;
}
