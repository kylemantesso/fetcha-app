export interface ICart {
  [key: string]: number;
}

export interface ICartRequest {
  cart: ICart;
  name?: string;
  table: string;
}
