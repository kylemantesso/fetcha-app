

export interface IVenue {
  public: {
    id: string;
    merchantId: string;
    covid: boolean;
    name: string;
    theme?: {
      logo?: string;
      header: string;
      brand: string;
      "accent-1": string;
    }
    tables: {
      [key: string]: {
        location: string;
        table: string;
      }
    }
  }
}
