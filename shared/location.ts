

export interface ILocation {
  id: string;
  name: string;
  suburb: string;
}
