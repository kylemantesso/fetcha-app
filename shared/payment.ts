export interface IPaymentRequest {
  nonce: string;
  amount: number;
  buyerVerificationToken: string;
  orderId: string;
  locationId: string;
  merchantId: string;
}
