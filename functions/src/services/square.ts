import * as SquareConnect from "square-connect";
import { TokenResponse } from "../index";
import { firebase } from "./firebase";
import { v4 as uuidv4 } from "uuid";
import { CreatePaymentRequest, Order, OrderLineItem } from "square-connect";
import { ILocation } from "../../../shared/location";
import { ICartRequest } from "../../../shared/cart";
import { IMenu, IMenuVariation } from "../../../shared/menu";
import { config } from "../config";
import { IPaymentRequest } from "../../../shared/payment";

interface IMerchant {
  merchant: {
    id: string;
    business_name: string;
    country: string;
    language_code: string;
    currency: string;
    status: string;
  };
}

const db = firebase.database();

const defaultClient = SquareConnect.ApiClient.instance;

defaultClient.basePath = config.square.base;

export const getMerchant = async (
  merchantId: string
): Promise<{ name: string; id: string }> => {
  const accessToken = await getAccessToken(merchantId);

  const oauth2 = defaultClient.authentications["oauth2"];
  oauth2.accessToken = accessToken;

  const apiInstance = new SquareConnect.MerchantsApi() as any;

  return apiInstance
    .retrieveMerchant(merchantId)
    .then(({ merchant: { business_name } }) => {
      return {
        name: business_name,
        id: webify(business_name),
      };
    });
};

export const getLocations = async (
  merchantId: string
): Promise<ILocation[]> => {
  const accessToken = await getAccessToken(merchantId);
  console.log("get locations");
  const oauth2 = defaultClient.authentications["oauth2"];
  oauth2.accessToken = accessToken;

  const apiInstance = new SquareConnect.LocationsApi();

  return apiInstance.listLocations().then((res) => {
    return res.locations.map(({ id, name, address: { locality } }) => ({
      id,
      suburb: locality,
      name,
    }));
  });
};

export const createOrder = async (
  merchantId: string,
  locationId: string,
  cartRequest: ICartRequest
) => {
  const accessToken = await getAccessToken(merchantId);

  const oauth2 = defaultClient.authentications["oauth2"];
  oauth2.accessToken = accessToken;

  const apiInstance = new SquareConnect.OrdersApi();

  console.log("CART REQUEST");
  console.log(cartRequest);

  const line_items: OrderLineItem[] = Object.keys(cartRequest.cart).map(
    (catalog_object_id) => {
      console.log(catalog_object_id);
      return {
        catalog_object_id,
        quantity: cartRequest.cart[catalog_object_id] + "",
      };
    }
  );

  const order: Order = {
    line_items,
    location_id: locationId,
    fulfillments: [
      {
        shipment_details: {
          shipping_note: "Deliver to table " + cartRequest.table,
          shipping_type: "Table service",
          recipient: {
            display_name: cartRequest.name
              ? cartRequest.name + ", table " + cartRequest.table
              : "Table " + cartRequest.table,
          },
        },
        type: "SHIPMENT",
      },
    ],
    metadata: {
      table: cartRequest.table,
      name: cartRequest.name || "Unknown",
    },
  };

  console.log("ORDER REQUEST");
  console.log(order);

  return apiInstance.createOrder(locationId, {
    order,
    idempotency_key: uuidv4(),
  });
};

export const createPayment = async (paymentRequest: IPaymentRequest) => {
  const accessToken = await getAccessToken(paymentRequest.merchantId);

  const oauth2 = defaultClient.authentications["oauth2"];
  oauth2.accessToken = accessToken;

  const apiInstance = new SquareConnect.PaymentsApi();

  const body: CreatePaymentRequest = {
    amount_money: {
      amount: parseInt(paymentRequest.amount + ""),
      currency: "AUD",
    },
    autocomplete: true,
    idempotency_key: uuidv4(),
    location_id: paymentRequest.locationId,
    order_id: paymentRequest.orderId,
    source_id: paymentRequest.nonce,
    verification_token: paymentRequest.buyerVerificationToken,
  };

  return apiInstance.createPayment(body);
};

export const getMenu = async (merchantId: string): Promise<IMenu> => {
  const accessToken = await getAccessToken(merchantId);

  const oauth2 = defaultClient.authentications["oauth2"];
  oauth2.accessToken = accessToken;

  const apiInstance = new SquareConnect.CatalogApi();

  const menu: IMenu = {};
  const catalogObjects = await apiInstance
    .listCatalog({ types: "ITEM,ITEM_VARIATION,CATEGORY,IMAGE" })
    .then((res) => res.objects);

  const items: SquareConnect.CatalogObject[] = [];
  const images: SquareConnect.CatalogObject[] = [];
  const variations: SquareConnect.CatalogObject[] = [];

  console.log(catalogObjects);

  catalogObjects.forEach((obj) => {
    if (obj.type === "CATEGORY") {
      menu[obj.id] = {
        name: obj.category_data!.name,
        slug: slugify(obj.category_data!.name),
        id: obj.id,
        items: [],
      };
    }
    if (obj.type === "ITEM") {
      items.push(obj);
    }
    if (obj.type === "IMAGE") {
      images.push(obj);
    }
    if (obj.type === "ITEM_VARIATION") {
      variations.push(obj);
    }
  });

  items.forEach((item) => {
    Object.keys(menu).forEach((categoryId) => {
      if (categoryId === item.item_data.category_id) {
        const variations: IMenuVariation[] = [];
        item.item_data.variations.forEach((variation) => {
          variations.push({
            id: variation.id,
            price: variation.item_variation_data.price_money.amount,
            name: variation.item_variation_data.name,
            slug: slugify(variation.item_variation_data.name),
            image: getImageUrl(variation.image_id, images),
          });
        });
        menu[categoryId].items.push({
          id: item.id,
          image: getImageUrl(item.image_id, images),
          name: item.item_data.name,
          slug: slugify(item.item_data.name),
          description: item.item_data.description,
          variations,
        });
      }
    });
  });

  console.log(JSON.stringify(menu));

  return menu;
};

const slugify = (text: string) =>
  text
    .toLowerCase()
    .replace(/ /g, "-")
    .replace(/[^\w-]+/g, "");

const webify = (text: string) =>
  text
    .toLowerCase()
    .replace(/ /g, "")
    .replace(/[^\w-]+/g, "");

const getImageUrl = (
  imageId: string,
  images: SquareConnect.CatalogObject[]
): string | undefined => {
  if (!imageId) {
    return undefined;
  }
  const image = images.find((image) => image.id === imageId);
  if (image) {
    return image.image_data.url;
  } else {
    return undefined;
  }
};

const getAccessToken = (merchantId): Promise<string> =>
  db
    .ref("auth/" + merchantId)
    .once("value")
    .then((snap) => (snap.val() as TokenResponse).access_token);
