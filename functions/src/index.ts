import * as functions from "firebase-functions";
import fetch from "node-fetch";
import {
  createOrder,
  createPayment,
  getLocations,
  getMenu,
  getMerchant,
} from "./services/square";
import { firebase } from "./services/firebase";
import { config } from "./config";
import { IVenue } from "../../shared/venue";
import { ICartRequest } from "../../shared/cart";
import { IPaymentRequest } from "../../shared/payment";

export interface TokenResponse {
  access_token: string;
  token_type: string;
  expires_at: string;
  merchant_id: string;
  refresh_token: string;
}

export const getVenueReference = (venueId: string) => `venues/${venueId}`;

export const payment = functions.https.onRequest(async (request, response) => {
  response.set("Access-Control-Allow-Origin", "*");
  const paymentRequest: IPaymentRequest = JSON.parse(request.body);

  createPayment(paymentRequest)
    .then((res) => {
      response.send(res);
    })
    .catch((error) => response.status(500).send(error));
});

export const order = functions.https.onRequest(async (request, response) => {
  response.set("Access-Control-Allow-Origin", "*");
  const params = request.path.split("/");
  const merchantId = params[1];
  const locationId = params[2];
  const cartRequest: ICartRequest = JSON.parse(request.body);

  createOrder(merchantId, locationId, cartRequest)
    .then((res) => {
      response.send(res);
    })
    .catch((error) => response.status(500).send(error));
});

export const locations = functions.https.onRequest(
  async (request, response) => {
    response.set("Access-Control-Allow-Origin", "*");
    const [merchantId] = request.path.split("/").slice(-1);

    getLocations(merchantId)
      .then((locations) => {
        response.send(locations);
      })
      .catch((e) => {
        response.send(e);
      });
  }
);

export const menu = functions.https.onRequest(async (request, response) => {
  response.set("Access-Control-Allow-Origin", "*");
  const db = firebase.database();

  const [venueId] = request.path.split("/").slice(-1);

  console.log("venueId", venueId);

  const venue: IVenue = await db
    .ref(getVenueReference(venueId))
    .once("value")
    .then((snap) => snap.val());

  console.log("venue", venue);

  getMenu(venue.public.merchantId)
    .then((menu) => {
      response.send(menu);
    })
    .catch((e) => {
      response.send(e);
    });
});

export const redirect = functions.https.onRequest((request, response) => {
  const db = firebase.database();

  console.log("QUERY PARAMS", request.query);
  const { code, state: uid } = request.query;

  console.log("CONFIG", config);

  const body = JSON.stringify({
    client_id: config.square.appId,
    client_secret: config.square.secret,
    code,
    grant_type: "authorization_code",
  });

  const headers = {
    "Content-Type": "application/json",
    "Square-Version": "2020-05-28",
  };

  fetch(config.square.tokenUrl, {
    method: "POST",
    headers,
    body,
  })
    .then((res) => {
      if (res.ok) {
        console.log("Token request OK");
        res.json().then((res: TokenResponse) => {
          console.log(res);
          db.ref(`auth/${res.merchant_id}`)
            .set(res)
            .then(() => {
              console.log("GET MERCHANT");
              getMerchant(res.merchant_id)
                .then((merchantData) => {
                  console.log("GET MERCHANT DATA", merchantData);

                  db.ref(getVenueReference(merchantData.id)).set({
                    public: {
                      covid: false,
                      id: merchantData.id,
                      merchantId: res.merchant_id,
                      name: merchantData.name,
                    },
                  });

                  db.ref(`${getVenueReference(merchantData.id)}/users`).push(
                    uid
                  );

                  db.ref(`users/${uid}`).set({
                    merchantId: res.merchant_id,
                    venueId: merchantData.id,
                  });
                })
                .catch((err) => {
                  console.log("GET MERCHANT ERROR");
                  console.log(err);
                });
            });
          response.redirect(config.square.redirectUrl);
        });
      } else {
        console.log(res);
        response.send("Not Ok");
      }
    })
    .catch((e) => {
      console.log(e);
      response.send(e);
    });
});
