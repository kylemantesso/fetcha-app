import * as functions from "firebase-functions";

export const config = {
  square: {
    appId: functions.config().square.appid,
    secret: functions.config().square.secret,
    base: functions.config().square.base,
    tokenUrl: functions.config().square.token_url,
    redirectUrl: functions.config().square.redirect_url,
  },
};
