import {Heading, Main } from "grommet";
import React from "react";
import { Spinner } from "../Spinner/Spinner";
import styled from "styled-components";

const PageMain = styled(Main)`
  max-width: 720px;
  margin: auto;
`

export const Page: React.FC<{ title?: string; loading?: boolean }> = ({
  children,
  title,
  loading = false,
}) => (
  <PageMain flex pad="medium" background={"background"} fill={true}>
    {loading ? (
      <Spinner />
    ) : (
      <>
        <Heading size="medium">{title}</Heading>
        {children}
      </>
    )}
  </PageMain>
);
