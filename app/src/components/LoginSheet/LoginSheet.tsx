import React, { useEffect, useState } from "react";
import {
  Button,
  Layer,
  Text,
  Box,
  Heading,
  FormField,
  TextInput,
} from "grommet";
import firebase from "firebase";

export const LoginSheet: React.FC<{ name: string; venueId: string }> = ({
  name,
  venueId,
}) => {
  const [show, setShow] = useState(true);
  const [details, setDetails] = useState<{
    name?: string;
    phone?: string;
    nameError?: string;
    phoneError?: string;
  }>({});

  const isValid = () => {
    setDetails({ ...details, nameError: undefined, phoneError: undefined });
    let nameError, phoneError;
    if (!details.name) {
      nameError = "Please enter your name";
    }

    if (!details.phone) {
      phoneError = "Please enter your phone number";
    }

    setDetails({ ...details, phoneError, nameError });

    return !nameError && !phoneError;
  };

  const handleSubmit = () => {
    const { name, phone } = details;
    if (isValid()) {
      firebase
        .database()
        .ref(`venues/${venueId}/contacts`)
        .push({ name, phone });
      sessionStorage.setItem("CHECKIN", Date.now() + "");
      setShow(false);
    }
  };

  return (
    <>
      {show && (
        <Layer
          responsive={false}
          full={"horizontal"}
          position="bottom"
          onEsc={() => setShow(false)}
          onClickOutside={() => setShow(false)}
        >
          <Box pad="medium">
            <Heading>Welcome to {name}</Heading>
            <Box gap="medium" pad={{ bottom: "xlarge" }}>
              <Text>
                To aid COVID-19 contact tracing, we must collect some
                information which is only shared with health authorities.
              </Text>
              <FormField error={details.nameError} label="Name">
                <TextInput
                  placeholder="John Smith"
                  onChange={(event) =>
                    setDetails({ ...details, name: event.target.value })
                  }
                />
              </FormField>
              <FormField error={details.phoneError} label="Phone number">
                <TextInput
                  placeholder="0401 123 456"
                  onChange={(event) =>
                    setDetails({ ...details, phone: event.target.value })
                  }
                />
              </FormField>
              <Button primary label="Submit" onClick={() => handleSubmit()} />
            </Box>
          </Box>
        </Layer>
      )}
    </>
  );
};
