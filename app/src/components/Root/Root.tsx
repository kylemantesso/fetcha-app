import React from "react";
import { Grommet } from "grommet";
import { Router } from "../Router/Router";
import { ThemeType } from "grommet/themes";

export const Root: React.FC = () => {
  const theme: ThemeType = {
    global: {
      focus: {
        border: {
          color: "accent-1",
        },
      },
      colors: {
        focus: "#FFF48B",
        brand: "#3D6EB5",
        "accent-1": "#EE285B",
        "accent-2": "#FFF48B",
        background: "#f6f6f6",
      },
    },
    button: {
      extend: "font-weight: 600;",
      border: {
        color: "accent-1",
      },
      primary: {
        color: "accent-1",
      },
    },
    heading: {
      font: {
        family: "fatfrank",
      },
    },
  };

  return (
    <Grommet theme={theme} id="grommet">
      <Router />
    </Grommet>
  );
};
