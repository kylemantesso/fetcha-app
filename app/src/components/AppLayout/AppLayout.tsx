import { Header } from "../Header/Header";
import React, { useContext } from "react";
import { Box, ResponsiveContext } from "grommet";

export const AppLayout: React.FC = ({ children }) => {
  const size = useContext(ResponsiveContext);
  return (
    <>
      <Header />
      <Box flex margin={{ top: size === "small" ? "56px" : "88px" }}>
        {children}
      </Box>
    </>
  );
};
