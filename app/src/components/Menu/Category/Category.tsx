import { Box, Grid, Heading, Image, Paragraph } from "grommet";
import React from "react";
import styled from "styled-components";
import { Link, useLocation } from "react-router-dom";
import {IMenuCategory} from "../../../../../shared/menu";

const MenuTitle = styled(Heading)`
  font-size: 16px;
  line-height: 24px;
`;

const MenuDescription = styled(Paragraph)`
  font-size: 14px;
  line-height: 18px;
`;

const MenuImage = styled(Image)`
  border-top-left-radius: 6px;
  border-top-right-radius: 6px;
  max-height: 140px;
`;

export const Category: React.FC<{ category: IMenuCategory }> = ({
  category,
}) => {
  const { pathname } = useLocation();
  return (
    <div key={category.id}>
      <Heading size="small">{category.name}</Heading>
      <Grid
        fill="vertical"
        columns={{
          count: 2,
          size: "xsmall"
        }}
        gap="small"
      >
        {category.items!.map((item) => (
          <Link  style={{textDecoration: 'none'}} key={item.id} to={`${pathname}/${category.slug}/${item.slug}`}>
            <Box fill round="small" background="brand"  elevation="medium">
              <MenuImage
                fit="cover"
                src={item.image || item.variations[0].image}
              />
              <Box pad="medium" >
                <MenuTitle level={2} margin="none">
                  {item.name}
                </MenuTitle>
                <MenuDescription margin="none">
                  {item.description}
                </MenuDescription>
              </Box>
            </Box>
          </Link>
        ))}
      </Grid>
    </div>
  );
};
