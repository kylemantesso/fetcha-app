import * as React from "react";
import { Box, Button } from "grommet";
import {IMenu} from "../../../../../shared/menu";

const CategoryButton: React.FC<{
  category: string;
  selected: boolean;
  label: string;
  onClick: (category: string) => void;
}> = ({ category, selected, label, onClick }) => (
  <Button
    margin={{bottom: "small"}}
    primary={selected}
    size="small"
    label={label}
    onClick={() => {
      onClick(category);
    }}
  />
);

export const CategoryList: React.FC<{
  selected: string;
  menu: IMenu;
  onSelect: (categoryId: string) => void;
}> = ({ menu, selected, onSelect }) => {
  return (
    <Box
      direction="row"
      gap="small"
      wrap={true}
    >
      <CategoryButton
        category={"all"}
        selected={selected === "all"}
        label={"All"}
        onClick={onSelect}
      />
      {Object.keys(menu).map((category) => (
        <CategoryButton
          key={category}
          category={category}
          selected={selected === category}
          label={menu[category].name}
          onClick={onSelect}
        />
      ))}
    </Box>
  );
};
