import React from "react";
import { Category } from "./Category/Category";
import {IMenu} from "../../../../shared/menu";

export const Menu: React.FC<{ menu: IMenu; selected: string }> = ({
  menu,
  selected,
}) => {
  let filteredMenu: IMenu = {};
  if (selected === "all") {
    filteredMenu = menu;
  } else {
    filteredMenu[selected] = menu[selected];
  }
  return (
    <div>
      {Object.keys(filteredMenu).map((categoryId) => (
        <Category key={categoryId} category={menu[categoryId]} />
      ))}
    </div>
  );
};
