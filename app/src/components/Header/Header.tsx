import React, { useContext } from "react";
import {
  Header as HeaderBase,
  Heading,
  Box,
  Text,
  Image,
  ResponsiveContext,
} from "grommet";
import { Cart, Gremlin, LinkPrevious } from "grommet-icons";
import styled from "styled-components";
import { CartContext } from "../../context/CartContext";
import { Link, useLocation, useHistory } from "react-router-dom";
import { HeaderContext } from "../../context/HeaderContext";
import { MenuContext } from "../../context/MenuContext";

const Circle = styled(Box)`
  border-radius: 50%;
  width: 20px;
  height: 20px;
  position: relative;
  bottom: 8px;
  right: 8px;
`;

const StyledHeader = styled(HeaderBase)`
  position: fixed;
  top: 0;
  width: 100%;
  min-height: 56px;
`;

export const Header: React.FC = () => {
  const { cart } = useContext(CartContext);
  const { backButton } = useContext(HeaderContext);
  const { venue } = useContext(MenuContext);

  const history = useHistory();
  const { pathname } = useLocation();
  const venueId = pathname.split("/")[1];
  const items = Object.values(cart).reduce((qty, acc) => qty + acc, 0);
  const size = useContext(ResponsiveContext);

  return (
    <StyledHeader background={venue?.theme?.header || "white"} pad="medium">
      {backButton ? (
        <LinkPrevious
          style={{ height: size === "small" ? "32px" : "40px" }}
          onClick={() => history.goBack()}
        />
      ) : (
        <Link
          style={{ textDecoration: "none", color: "#fff" }}
          to={`/${venueId}`}
        >
          <Box align="start" direction="row" gap="xsmall">
            {venue ? (
              venue?.theme?.logo ? (
                <Image height={48} src={venue.theme.logo} fit="contain" />
              ) : (
                <Heading color="brand" margin="none" size={"small"}>
                  {venue.name}
                </Heading>
              )
            ) : (
              <Heading color="brand" margin="none" size={"small"}>
                Fetcha
              </Heading>
            )}
          </Box>
        </Link>
      )}
      <Link style={{ textDecoration: "none" }} to={`/${venueId}/checkout`}>
        {items !== 0 && (
          <Box align="center" direction="row">
            <Cart />
            <Circle
              align="center"
              justify="center"
              direction="column"
              background="accent-1"
            >
              <Text
                color="white"
                size="small"
                style={{ textDecoration: "none", fontWeight: 800 }}
              >
                {items}
              </Text>
            </Circle>
          </Box>
        )}
      </Link>
    </StyledHeader>
  );
};
