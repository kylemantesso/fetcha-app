import {Box} from "grommet";
import styled from "styled-components";


export const BottomFixed = styled(Box)`
  position: fixed;
  bottom: 0;
`
