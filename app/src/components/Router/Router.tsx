import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Home } from "../../screens/Home/Home";
import { AppLayout } from "../AppLayout/AppLayout";
import { CartProvider } from "../../context/CartContext";
import { MenuProvider } from "../../context/MenuContext";
import { Venue } from "../../routes/Venue/Venue";
import { Merchant } from "../../routes/Merchant/Merchant";
import { HeaderProvider } from "../../context/HeaderContext";
import { ScrollToTop } from "../ScrollToTop/ScrollToTop";

export const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <HeaderProvider>
        <ScrollToTop />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/merchant">
            <AppLayout>
              <Merchant />
            </AppLayout>
          </Route>
          <Route path="/:venueId">
            <CartProvider>
              <MenuProvider>
                <AppLayout>
                  <Venue />
                </AppLayout>
              </MenuProvider>
            </CartProvider>
          </Route>
        </Switch>
      </HeaderProvider>
    </BrowserRouter>
  );
};
