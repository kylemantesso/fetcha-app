import React from "react";
import ColorPicker from "rc-color-picker";
import "rc-color-picker/assets/index.css";
import { useObjectVal } from "react-firebase-hooks/database";
import { IVenue } from "../../../../../shared/venue";
import firebase from "firebase";
import { getVenueReference } from "../../../services/firebase";
import { Box } from "grommet";
import { Spinner } from "../../Spinner/Spinner";

export const Theme: React.FC<{ venueId: string }> = ({ venueId }) => {
  const [venue, loadingVenue] = useObjectVal<IVenue["public"]>(
    firebase.database().ref(getVenueReference(venueId))
  );

  const onPickColor = (color: any) => {
    console.log(color);
  };

  return (
    <Box>
      {loadingVenue ? (
        <Spinner />
      ) : (
        <Box>
          <ColorPicker
            animation="slide-up"
            color={"#36c"}
            onChange={onPickColor}
          />{" "}
        </Box>
      )}
    </Box>
  );
};
