import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase";
import {Box, Heading, Paragraph} from "grommet";
import {Page} from "../../components/Page/Page";

// Configure FirebaseUI.
const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: "popup",
  // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: "/merchant/link",
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
  ],
};

export const SignUp = () => {
  return (
    <Page title="Merchant Sign Up">
      <Box direction="row-responsive" align="center">
        <Paragraph textAlign="center">It's easy! Simply sign up with a Fetcha account and then link your Square account and you're good to go!</Paragraph>
        <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
      </Box>
    </Page>
  );
};
