import React, { useContext, useState } from "react";
import {
  Box,
  Button,
  Heading,
  Image,
  Main,
  Paragraph,
  Text,
  RadioButton,
} from "grommet";
import { useParams } from "react-router-dom";
import { CartContext } from "../../context/CartContext";
import { ActionType } from "../../context/reducers";
import { MenuContext } from "../../context/MenuContext";
import { Spinner } from "../../components/Spinner/Spinner";
import {HeaderContext} from "../../context/HeaderContext";
import {IMenu, IMenuItem} from "../../../../shared/menu";

const formatCurrency = (amount: number) => {
  const dollars = amount / 100;
  return dollars.toLocaleString("en-AU", {
    style: "currency",
    currency: "AUD",
  });
};

const findItem = (categorySlug: string, itemSlug: string, menu: IMenu) => {
  let foundItem;
  Object.keys(menu).forEach((categoryId) => {
    if (menu[categoryId].slug === categorySlug) {
      menu[categoryId].items!.forEach((item) => {
        if (item.slug === itemSlug) {
          foundItem = item;
        }
      });
    }
  });
  if (foundItem) {
    return foundItem as IMenuItem;
  } else {
    return undefined;
  }
};

export const Item: React.FC = () => {
  const context = useContext(CartContext);
  const {setBackButton} = useContext(HeaderContext);
  setBackButton(true);

  const { menu, loading } = useContext(MenuContext);

  const [selectedVariationId, setSelectedVariationId] = useState();


  let item: IMenuItem | undefined = undefined;
  const { categorySlug, itemSlug } = useParams();

  const handleAddVariation = () => {
    context.dispatch({
      type: ActionType.ADD_VARIATION,
      payload: {
        variationId: selectedVariationId,
      },
    });
  };

  if (!loading && menu) {
    item = findItem(categorySlug, itemSlug, menu);
    if (!selectedVariationId) {
      setSelectedVariationId(item!.variations![0].id);
    }
  }

  return (
    <Main>
      {loading ? (
        <Spinner />
      ) : item ? (
        <Box flex>
          <Image
            fit="cover"
            src={item.image || item.variations[0].image}
            style={{ maxHeight: "280px" }}
          />
          <Box pad="medium" gap="large">
            <Box pad="none" gap="medium">
              <Heading margin="none">{item.name}</Heading>
              <Paragraph margin="none">{item.description}</Paragraph>
              {item.variations.map((variation) => (
                <Box key={variation.id} direction="row" justify="between" pad="small">
                  <RadioButton
                    name={variation.slug}
                    checked={variation.id === selectedVariationId}
                    label={variation.name}
                    onClick={() => {
                      setSelectedVariationId(variation.id);
                    }}
                  />
                  <Text>{formatCurrency(variation.price)}</Text>
                </Box>
              ))}
            </Box>
            <Button
              primary
              label="Add to cart"
              size="large"
              onClick={() => {
                handleAddVariation();
              }}
            />
          </Box>
        </Box>
      ) : null}
    </Main>
  );
};
