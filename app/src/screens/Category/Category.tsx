import React, { useContext } from "react";
import { Box, Button, Heading, Image, Paragraph, Text } from "grommet";
import { useLocation } from "react-router-dom";
import { CartContext } from "../../context/CartContext";
import {HeaderContext} from "../../context/HeaderContext";
import {IMenu, IMenuItem} from "../../../../shared/menu";

const formatCurrency = (amount: number) => {
  const dollars = amount / 100;
  return dollars.toLocaleString("en-AU", {
    style: "currency",
    currency: "AUD",
  });
};

export const Category: React.FC<{category: IMenu}> = () => {
  const location = useLocation<{ item: IMenuItem }>();
  const context = useContext(CartContext);

  const { item } = location.state;
  const {setBackButton} = useContext(HeaderContext);
  setBackButton(false);

  const handleAddItem = (itemId: string) => {
    console.log('Clicked Add item: ' + itemId);
  }

  return (
    <Box flex>
      <Image fit="cover" src={item.image || item.variations[0].image} />
      <Box pad="medium" gap="medium">
        <Heading margin="none">{item.name}</Heading>
        <Paragraph margin="none">{item.description}</Paragraph>
        {item.variations.map((variation) => (
          <Box direction="row" justify="between">
            <Text>{variation.name}</Text>
            <Text>{formatCurrency(+variation.price)}</Text>
          </Box>
        ))}
      </Box>
      <Box flex justify="end" pad="large">
        <Button
          primary
          label="Add to cart"
          size="large"
          onClick={() => {
            handleAddItem(item.id);
          }}
        />
      </Box>
    </Box>
  );
};
