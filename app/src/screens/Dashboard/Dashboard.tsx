import React, { useEffect, useState } from "react";

import {
  Box,
  Button,
  CheckBox,
  FormField,
  Heading,
  Main,
  Select,
  Tab,
  Tabs,
  TextInput,
} from "grommet";
import { useObjectVal } from "react-firebase-hooks/database";
import firebase from "firebase";
import {
  fetchLocations,
  getTableReference,
  getUserReference,
  getVenueReference,
} from "../../services/firebase";
import { Spinner } from "../../components/Spinner/Spinner";

import QRCode from "qrcode.react";
import { IUser } from "../../../../shared/user";
import { config } from "../../config/config";
import { Page } from "../../components/Page/Page";
import { IVenue } from "../../../../shared/venue";
import { Theme } from "../../components/Merchant/Theme/Theme";

const BASE_URL = config.firebase.HOSTING_URL;

export const Dashboard: React.FC<{ authUser?: firebase.User }> = ({
  authUser,
}) => {
  const [user, loading] = useObjectVal<IUser>(
    firebase.database().ref(getUserReference(authUser!.uid))
  );

  const [table, setTable] = useState();
  const [location, setLocation] = useState();
  const [locationData, setLocationData] = useState();

  const [locations, setLocations] = useState<
    { label: string; value: string }[]
  >();
  const [tables, setTables] = useState<{ location: string; table: string }[]>();

  useEffect(() => {
    if (user && !locations) {
      firebase
        .database()
        .ref(getTableReference(user!.venueId))
        .on("value", (snap) => {
          const dbTables = snap.val();
          if (dbTables) {
            setTables(
              Object.entries(dbTables).map(([key, val]: any) => ({ ...val }))
            );
          }
        });

      fetchLocations(user.merchantId).then((res) => {
        if (res) {
          let locs: { label: string; value: string }[] = [];
          const locData: Record<string, string> = {};
          res.forEach((location) => {
            locs.push({
              label: `${location.name}`,
              value: location.id,
            });
            locData[location.id] = location.name;
          });
          setLocationData(locData);
          setLocation(locs[0].value);
          setLocations(locs);
        }
      });
    }
  }, [user, locations]);

  const addTable = () => {
    console.log("table", table);
    firebase
      .database()
      .ref(getTableReference(user!.venueId))
      .push({ table, location });
  };

  return (
    <Page loading={!authUser || loading || !locations}>
      <Tabs>
        <Tab title="Tables">
          <Heading size="small">Tables</Heading>
          <Box gap="large">
            <FormField label="Table description">
              <TextInput
                value={table}
                onChange={(event) => setTable(event.target.value)}
                placeholder="1"
              />
            </FormField>
            <FormField label="Location">
              {locations && (
                <Select
                  options={locations}
                  labelKey="label"
                  valueKey={{ key: "value", reduce: true }}
                  value={location}
                  onChange={({ value: nextValue }) => setLocation(nextValue)}
                />
              )}
            </FormField>
            <Button
              primary
              onClick={() => addTable()}
              label="Generate QR Code"
            />
          </Box>
          {tables && locationData
            ? tables.map(({ location, table }) => (
                <Box key={location + table}>
                  <Box
                    gap="small"
                    pad={{ bottom: "small", left: "xlarge", right: "xlarge" }}
                  >
                    <Heading textAlign="center" size="small">
                      {locationData[location]}
                      <br />
                      Table {table}
                    </Heading>
                    <QRCode
                      style={{ width: "100%", height: "100%" }}
                      value={`${BASE_URL}/${user?.venueId}?table=${table}&location=${location}`}
                    />
                  </Box>
                </Box>
              ))
            : null}
        </Tab>
        <Tab title="Contact tracing">
          <Heading size="small">Contact Tracing</Heading>
          <CheckBox checked={true} label="Ask for contact tracing details" />
        </Tab>
        <Tab title="Theme">
          {user?.venueId && <Theme venueId={user?.venueId} />}
        </Tab>
      </Tabs>
    </Page>
  );
};
