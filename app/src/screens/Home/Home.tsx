import React, { useContext } from "react";
import { HeaderContext } from "../../context/HeaderContext";
import {
  Box,
  Button,
  Footer,
  Header,
  Heading,
  Image,
  Paragraph,
  ResponsiveContext,
} from "grommet";
import Logo from "./logo-blue.svg";
import Spritz from "./spritz.svg";
import Lemon from "./lemon.svg";
import Beer from "./beer.svg";
import Wine from "./wine.svg";

import Qr from "./qr-icon.svg";

import styled from "styled-components";
import { Link } from "react-router-dom";
import { Catalog, Deliver, Group, Square } from "grommet-icons";

const SlantBox = styled(Box)`
  --transform-translate-x: 0;
  --transform-translate-y: 0;
  --transform-rotate: 0;
  --transform-skew-x: -12deg;
  --transform-skew-y: 0;
  --transform-scale-x: 1;
  --transform-scale-y: 1;
  transform: translateX(var(--transform-translate-x))
    translateY(var(--transform-translate-y)) rotate(var(--transform-rotate))
    skewX(var(--transform-skew-x)) skewY(var(--transform-skew-y))
    scaleX(var(--transform-scale-x)) scaleY(var(--transform-scale-y));
`;

export const Home: React.FC = () => {
  const { setBackButton } = useContext(HeaderContext);
  const size = useContext(ResponsiveContext);

  setBackButton(false);
  return (
    <Box>
      <Header background="white" pad="medium">
        <Image src={Logo} height={size === "small" ? 40 : 56} />
        <Link to="/merchant/signup">
          <Button primary label="Sign Up" />
        </Link>
      </Header>
      <Box
        background="background"
        pad={size === "small" ? "large" : "xlarge"}
        justify="around"
        align="center"
        margin={{ top: "-1px", bottom: "-1px" }}
      >
        <Heading margin="none">Just sit,</Heading>
        <SlantBox background="brand" pad="small">
          <Heading margin="none">we'll fetch</Heading>
        </SlantBox>
        <Paragraph textAlign="center">
          <strong>Fetcha</strong>, the easiest way to offer table service to
          your customers
        </Paragraph>
        <Box direction="row" gap="medium">
          <Image src={Beer} height={80} />
          <Image src={Spritz} height={80} />
          <Image src={Wine} height={80} />
          <Image src={Lemon} height={80} />
        </Box>
      </Box>

      <Box
        background="white"
        direction="row-responsive"
        pad="large"
        gap="large"
      >
        <Box style={{ height: size === "small" ? 200 : 500 }} fill="horizontal">
          <Image fit="contain" src="/assets/screens.png" />
        </Box>
        <Box fill="horizontal" align="center" justify="center">
          <Box justify="center">
            <Heading textAlign="center" margin="none">
              Digital menus
            </Heading>
            <SlantBox background="accent-1" pad="small">
              <Heading textAlign="center" margin="none">
                at your fingertips
              </Heading>
            </SlantBox>
          </Box>
          <Paragraph textAlign="center">
            Unleash the items already in your Square Point of Sale to deliver
            customers an easy to use online menu, making table service a breeze!
          </Paragraph>
          <Link to="/flyliebar">
            <Button color="brand" primary label="See one in action!"></Button>
          </Link>
        </Box>
      </Box>

      <Box
        background="background"
        direction={size === "small" ? "column-reverse" : "row"}
        pad="large"
        gap="large"
      >
        <Box fill="horizontal" align="center" justify="center">
          <Box justify="center">
            <Heading textAlign="center" margin="none">
              Accept
            </Heading>
            <SlantBox background="accent-2" pad="small">
              <Heading textAlign="center" margin="none">
                online payments
              </Heading>
            </SlantBox>
          </Box>
          <Paragraph textAlign="center">
            Your customers will have a seamless online experience, giving them
            multiple ways to pay right from their table.
          </Paragraph>
        </Box>
        <Box style={{ height: size === "small" ? 200 : 540 }} fill="horizontal">
          <Image fit="contain" src="/assets/screens-2.png" />
        </Box>
      </Box>

      <Box
        background="white"
        direction="row-responsive"
        pad="large"
        gap="large"
      >
        <Box style={{ height: size === "small" ? 200 : 540 }} fill="horizontal">
          <Image fit="contain" src="/assets/screens-3.png" />
        </Box>
        <Box fill="horizontal" align="center" justify="center">
          <Box justify="center">
            <Heading textAlign="center" margin="none">
              All your orders in
            </Heading>
            <SlantBox background="brand" pad="small">
              <Heading textAlign="center" margin="none">
                Square Point of Sale
              </Heading>
            </SlantBox>
          </Box>
          <Paragraph textAlign="center">
            Free your staff from taking orders, giving them more time for true
            customer service.
          </Paragraph>
        </Box>
      </Box>

      <Box align="center" pad="large" gap="large">
        <Heading>How it works</Heading>

        <Box direction="row-responsive" gap="xlarge">
          <Box align="center" fill>
            <Box background="accent-1" round="50%" pad="medium" align="center">
              <Box pad="small">
                <Image height="96px" src={Qr} />
              </Box>
            </Box>
            <Heading textAlign="center" size="small">
              Scan QR Code
            </Heading>
            <Paragraph textAlign="center">
              Customers simply scan the QR code on the table to view your online
              menu, <strong>no app required!</strong>
            </Paragraph>
          </Box>

          <Box align="center" fill>
            <Box background="accent-1" round="50%" pad="medium" align="center">
              <Box pad="small">
                <Group size="xlarge" />
              </Box>
            </Box>
            <Heading textAlign="center" size="small">
              Order online
            </Heading>
            <Paragraph textAlign="center">
              Customers place orders online and can pay with Apple Pay, Google
              Pay or Credit Card.
            </Paragraph>
          </Box>

          <Box align="center" fill>
            <Box background="accent-1" round="50%" pad="medium" align="center">
              <Box pad="small">
                <Square size="xlarge" />
              </Box>
            </Box>
            <Heading textAlign="center" size="small">
              Sqaure POS
            </Heading>
            <Paragraph textAlign="center">
              All of your orders and payments go straight into your Square Point
              of Sale for fulfilment.
            </Paragraph>
          </Box>
        </Box>
      </Box>

      <Box align="center" pad="large" gap="large">
        <Heading>Our Partners</Heading>
        <Box direction="row-responsive" gap="large">
          <Box round="small" fill background="white" elevation="large">
            <Image fit="contain" src={"/assets/chei-wen-logo.png"} />
            <Paragraph textAlign="center">
              Chei Wen Wine Bar - Ivanhoe, VIC
            </Paragraph>
          </Box>
          <Box round="small" fill background="white" elevation="large">
            <Image fit="contain" src={"/assets/fly-lie-logo.png"} />
            <Paragraph textAlign="center">Fly Lie Bar - Kew, VIC</Paragraph>
          </Box>
        </Box>
      </Box>

      <Footer justify="center" pad="medium">
        <Paragraph>Copyright © 2020 Fetcha. All Rights Reserved</Paragraph>
      </Footer>
    </Box>
  );
};
