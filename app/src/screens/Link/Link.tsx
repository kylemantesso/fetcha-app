import React from "react";

import { Button, Heading, Main, Paragraph } from "grommet";
import firebase from "firebase";
import { config } from "../../config/config";
import { Page } from "../../components/Page/Page";

export const Link: React.FC<{ authUser?: firebase.User }> = ({ authUser }) => {
  return (
    <Page title="Link your account">
      <Paragraph>Now we need to link your Square account</Paragraph>
      <Button
        style={{ textAlign: "center" }}
        href={`${config.square.AUTHORIZE_URL}&state=${authUser!.uid}`}
        label="Link my Square account"
        primary
      />
    </Page>
  );
};
