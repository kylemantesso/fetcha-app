import React, { useContext } from "react";
import { Box, Button, Heading, Paragraph } from "grommet";
import { useParams, Link } from "react-router-dom";
import { StatusGood } from "grommet-icons";
import { MenuContext } from "../../context/MenuContext";

const formatCurrency = (amount: number) => {
  const dollars = amount / 100;
  return dollars.toLocaleString("en-AU", {
    style: "currency",
    currency: "AUD",
  });
};

export const Receipt: React.FC = () => {
  const { amount } = useParams();
  const { venue } = useContext(MenuContext);

  return (
    <Box pad={"medium"} align="center" justify={"center"} fill={true} flex>
      <StatusGood color="accent-1" size={"xlarge"} />
      <Heading size="medium">Thanks!</Heading>
      <Heading size="small">{formatCurrency(amount)} PAID</Heading>
      <Paragraph textAlign="center">
        Your order has been received and will be delivered to you shortly!
      </Paragraph>
      <Link to={`/${venue && venue!.id}`}>
        <Button size="large" label="Back to menu" />
      </Link>
    </Box>
  );
};
