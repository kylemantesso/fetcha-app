import React, { useEffect, useState } from "react";
import { Spinner } from "../../components/Spinner/Spinner";
import { Main } from "grommet";
import { useParams, Redirect } from "react-router-dom";
import { getUserReference } from "../../services/firebase";
import firebase from "firebase";

export const Setup: React.FC<{ authUser?: firebase.User }> = ({ authUser }) => {
  const { merchantId } = useParams();
  const [linked, setLinked] = useState(false);
  const user = { merchantId };

  useEffect(() => {
    firebase
      .database()
      .ref(getUserReference(authUser!.uid!))
      .set(user)
      .then(() => {
        setLinked(true);
      });
  }, []);

  return (
    <Main>{linked ? <Redirect to="/merchant/dashboard" /> : <Spinner />}</Main>
  );
};


