import React, { useContext, useState } from "react";
import { Menu as MenuList } from "../../components/Menu/Menu";
import { Box, Heading, Main } from "grommet";
import { CategoryList } from "../../components/Menu/CategoryList/CategoryList";
import { Spinner } from "../../components/Spinner/Spinner";
import { MenuContext } from "../../context/MenuContext";
import { HeaderContext } from "../../context/HeaderContext";
import { LoginSheet } from "../../components/LoginSheet/LoginSheet";
import { useQuery } from "../../hooks/useQuery";
import { Page } from "../../components/Page/Page";

export const Menu: React.FC = () => {
  const { menu, venue, loading } = useContext(MenuContext);
  const [selectedCategory, setSelectedCategory] = useState("all");
  const { setBackButton } = useContext(HeaderContext);

  let showCheckin = false;
  const checkin = sessionStorage.getItem("CHECKIN");
  if (checkin) {
    const diff = Date.now() - parseInt(checkin);
    if (diff > 8 * 60 * 60 * 1000) {
      sessionStorage.removeItem("CHECKIN");
      showCheckin = true;
    }
  } else {
    showCheckin = true;
  }

  const query = useQuery();
  const table = query.get("table");
  const location = query.get("location");

  if (table) {
    sessionStorage.setItem("TABLE", table);
  }

  if (location) {
    sessionStorage.setItem("LOCATION", location);
  }

  setBackButton(false);
  return (
    <Page title={venue?.name} loading={loading}>
      {menu && venue && (
        <Box>
          <CategoryList
            selected={selectedCategory}
            menu={menu!}
            onSelect={setSelectedCategory}
          />
          <MenuList selected={selectedCategory} menu={menu!} />
          {venue!.covid && showCheckin && (
            <LoginSheet venueId={venue!.id} name={venue!.name} />
          )}
        </Box>
      )}
    </Page>
  );
};
