import React, { useContext, useEffect, useState } from "react";
import {
  Box,
  Button,
  FormField,
  Heading,
  Select,
  Text,
  TextInput,
} from "grommet";
import { CartContext } from "../../context/CartContext";
import { MenuContext } from "../../context/MenuContext";
import { HeaderContext } from "../../context/HeaderContext";
import { AddCircle, SubtractCircle } from "grommet-icons";
import { ActionType } from "../../context/reducers";
import { fetchLocations, fetchOrder } from "../../services/firebase";
import { ICartRequest } from "../../../../shared/cart";
import { useHistory } from "react-router-dom";
import { Page } from "../../components/Page/Page";

const formatCurrency = (amount: number) => {
  const dollars = amount / 100;
  return dollars.toLocaleString("en-AU", {
    style: "currency",
    currency: "AUD",
  });
};

export const Checkout: React.FC = () => {
  const history = useHistory();

  const { cart, dispatch } = useContext(CartContext);
  const { menu, venue, loading } = useContext(MenuContext);
  const { setBackButton } = useContext(HeaderContext);

  const [foundLocation, setFoundLocation] = useState();
  const [locationById, setLocationById] = useState<Record<string, string>>({});
  const [locations, setLocations] = useState<
    { label: string; value: string }[]
  >();

  const tableStorage = sessionStorage.getItem("TABLE") || undefined;
  const locationStorage = sessionStorage.getItem("LOCATION") || undefined;

  const [table, setTable] = useState(tableStorage);
  const [tableError, setTableError] = useState();

  const [location, setLocation] = useState(locationStorage);
  const [locationError, setLocationError] = useState();

  useEffect(() => {
    if (!loading) {
      fetchLocations(venue!.merchantId).then((res) => {
        if (res) {
          let locs: { label: string; value: string }[] = [];
          const locData: Record<string, string> = {};
          res.forEach((loc) => {
            locs.push({
              label: `${loc.name}`,
              value: loc.id,
            });
            locData[loc.id] = loc.name;
            if (loc.id === locationStorage) {
              setFoundLocation(`${loc.name}`);
            }
          });

          setLocationById(locData);
          setLocations(locs);
        }
      });
    }
  }, [loading]);

  setBackButton(false);

  const [itemList, setItemList] = useState();
  const [total, setTotal] = useState();

  const itemVariations = Object.keys(cart);

  useEffect(() => {
    const tempItemList: any = {};
    let runningTotal = 0;
    if (!loading && menu) {
      Object.keys(menu).forEach((categoryId) => {
        menu[categoryId].items.forEach((item) => {
          item.variations.forEach((variation) => {
            if (itemVariations.includes(variation.id)) {
              if (!tempItemList[item.name]) {
                tempItemList[item.name] = {
                  variations: [],
                };
              }
              tempItemList[item.name].variations.push({
                id: variation.id,
                name: variation.name,
                price: variation.price,
                qty: cart[variation.id],
              });
              runningTotal += variation.price * cart[variation.id];
            }
          });
        });
      });

      setItemList(tempItemList);
      setTotal(runningTotal);
    }
  }, [loading, menu, cart]);

  const handleCheckout = () => {
    setTableError(undefined);
    setLocationError(undefined);
    if (table && location) {
      const cartRequest: ICartRequest = {
        table,
        cart,
      };

      fetchOrder({
        merchantId: venue!.merchantId,
        locationId: location,
        cartRequest,
      }).then((res) => {
        console.log(res);
        dispatch({ type: ActionType.CLEAR, payload: {} });
        history.push(
          `payment/${res.order.id}/${venue!.merchantId}/${
            res.order.location_id
          }/${res.order.total_money.amount}`
        );
      });
    } else {
      if (!table) {
        setTableError("Please enter your table number");
      }

      if (!location) {
        setLocationError("Please select your location");
      }
    }
  };

  const renderItems = () => {
    return Object.keys(itemList).map((itemName: string) => (
      <Box key={itemName}>
        <Text weight="bold">{itemName}</Text>
        {itemList[itemName].variations.map((variation: any) => (
          <Box key={variation.name} direction="row" pad="small" align="center">
            <Box flex>
              <Text size="medium">{variation.name}</Text>
            </Box>
            <Box direction="row" align="center">
              <Button
                onClick={() => {
                  dispatch({
                    type: ActionType.ADD_VARIATION,
                    payload: {
                      variationId: variation.id,
                    },
                  });
                }}
                icon={<AddCircle />}
              />
              <Text size="medium">{variation.qty}</Text>
              <Button
                onClick={() => {
                  dispatch({
                    type: ActionType.REMOVE_VARIATION,
                    payload: {
                      variationId: variation.id,
                    },
                  });
                }}
                icon={<SubtractCircle />}
              />
            </Box>
            <Box basis="1/4" align="end">
              <Text>{formatCurrency(variation.price * variation.qty)}</Text>
            </Box>
          </Box>
        ))}
      </Box>
    ));
  };
  return (
    <Page title="Checkout" loading={loading || !itemList || !locations}>
      {itemList && locations && (
        <Box gap="large" fill="horizontal">
          {locationStorage && tableStorage && foundLocation && table && (
            <Heading margin="none" size="small">
              {foundLocation}<br />Table {table}
            </Heading>
          )}
          <Box gap="medium" border="bottom">
            {renderItems()}
          </Box>
          <Box direction="row" justify="between">
            <Text weight="bold">Total</Text>
            <Text weight="bold">{formatCurrency(total)}</Text>
          </Box>
          {(!locationStorage || !tableStorage) && (
            <Box gap="large">
              <FormField error={tableError} label="Table number">
                <TextInput
                  value={table}
                  onChange={(event) => setTable(event.target.value)}
                  placeholder="1"
                />
              </FormField>
              <FormField error={locationError} label="Location">
                {locations && (
                  <Select
                    options={locations}
                    labelKey="label"
                    valueKey={{ key: "value", reduce: true }}
                    value={location}
                    onChange={({ value: nextValue }) => setLocation(nextValue)}
                  />
                )}
              </FormField>
            </Box>
          )}
          <Button
            primary
            label="Checkout"
            size="large"
            onClick={() => handleCheckout()}
          />
        </Box>
      )}
    </Page>
  );
};
