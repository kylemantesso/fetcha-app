import React, { useEffect, useState } from "react";
import { fetchMenu, getVenueReference } from "../services/firebase";
import { IMenuContextState } from "./types";
import { useObjectVal } from "react-firebase-hooks/database";
import firebase from "firebase";
import {IVenue} from "../../../shared/venue";
import {IMenu} from "../../../shared/menu";

const initialState: IMenuContextState = {
  loading: true,
};

export const MenuContext = React.createContext<IMenuContextState>({
  ...initialState,
});

export const MenuProvider: React.FC = ({ children }) => {
  const venueId = window.location.pathname.split("/")[1];

  const [venue] = useObjectVal<IVenue["public"]>(
    firebase.database().ref(getVenueReference(venueId))
  );

  const [menu, setMenu] = useState<IMenu>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string>();


  useEffect(() => {
    const doFetch = async () => {
      const menu = await fetchMenu(venueId);
      if (menu) {
        setMenu(menu);
      } else {
        setError("Menu is not defined");
      }
    };
    console.log("menu", menu);
    if (!menu) {
      doFetch();
    }
  }, [venueId, menu]);

  useEffect(() => {
    if(menu && venue) {
      setLoading(false)
    }
  }, [menu, venue])

  return (
    <MenuContext.Provider value={{ menu, loading, error, venue }}>
      {children}
    </MenuContext.Provider>
  );
};
