import React, {useEffect, useReducer} from "react";
import { reducer, Reducers } from "./reducers";
import {ICart} from "../../../shared/cart";

const initialState: ICart = {};

export const CART_STORAGE_KEY = 'cart';

export const CartContext = React.createContext<{
  cart: ICart;
  dispatch: React.Dispatch<Reducers>;
}>({ cart: initialState, dispatch: () => null });

export const CartProvider: React.FC = ({ children }) => {
  let localState = undefined;
  const cartStorage = sessionStorage.getItem(CART_STORAGE_KEY);

  if(cartStorage) {
    localState = JSON.parse(cartStorage);
  }

  const [cart, dispatch] = useReducer(reducer, localState || initialState);
  useEffect(() => {
    sessionStorage.setItem(CART_STORAGE_KEY, JSON.stringify(cart));
  }, [cart]);
  return (
    <CartContext.Provider value={{ cart, dispatch }}>
      {children}
    </CartContext.Provider>
  );
};
