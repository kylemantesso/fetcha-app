// eslint-disable-next-line @typescript-eslint/no-explicit-any

import { ICart } from "../../../shared/cart";
import {CART_STORAGE_KEY} from "./CartContext";

type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
      }
    : {
        type: Key;
        payload: M[Key];
      };
};

export enum ActionType {
  REMOVE_VARIATION = "REMOVE_VARIATION",
  ADD_VARIATION = "ADD_VARIATION",
  CLEAR = "CLEAR",
}

type ActionPayload = {
  [ActionType.ADD_VARIATION]: {
    variationId: string;
  };
  [ActionType.REMOVE_VARIATION]: {
    variationId: string;
  };
  [ActionType.CLEAR]: {};
};
export type Reducers = ActionMap<ActionPayload>[keyof ActionMap<ActionPayload>];

export const reducer = (state: ICart, action: Reducers): ICart => {
  switch (action.type) {
    case ActionType.ADD_VARIATION: {
      const { variationId } = action.payload;
      let amount = state[variationId] || 0;
      amount++;
      return { ...state, [variationId]: amount };
    }
    case ActionType.REMOVE_VARIATION: {
      const { variationId } = action.payload;
      let amount = state[variationId] || 0;
      amount--;
      if (amount < 1) {
        delete state[variationId];
      } else {
        state[variationId] = amount;
      }
      return { ...state };
    }
    case ActionType.CLEAR: {
      sessionStorage.removeItem(CART_STORAGE_KEY);
      return {};
    }
    default:
      console.error("No action defined", action);
      return state;
  }
};
