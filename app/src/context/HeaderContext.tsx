import React, { useState } from "react";

interface IHeaderContextState {
  backButton: boolean;
  setBackButton: (backButton: boolean) => void;
}

const initialState: IHeaderContextState = {
  backButton: false,
  setBackButton: () => {},
};

export const HeaderContext = React.createContext<IHeaderContextState>({
  ...initialState,
});

export const HeaderProvider: React.FC = ({ children }) => {
  const [backButton, setBackButton] = useState<boolean>(false);
  return (
    <HeaderContext.Provider value={{ backButton, setBackButton }}>
      {children}
    </HeaderContext.Provider>
  );
};
