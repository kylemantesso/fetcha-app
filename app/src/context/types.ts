import {IMenu} from "../../../shared/menu";
import {IVenue} from "../../../shared/venue";


export interface IMenuContextState {
  menu?: IMenu;
  loading: boolean;
  error?: string;
  venue?: IVenue['public'];
}
