import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import firebase from "firebase";
import { Root } from "./components/Root/Root";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyA1qdMpRqyfIHoKoAh-rjPOHtBZz_SyQSA",
  authDomain: "bar-mate-au.firebaseapp.com",
  databaseURL: "https://bar-mate-au.firebaseio.com",
  projectId: "bar-mate-au",
  storageBucket: "bar-mate-au.appspot.com",
  messagingSenderId: "298567518901",
  appId: "1:298567518901:web:4ba1636acb21161136ccfc",
  measurementId: "G-S3JEMMKBS2",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

ReactDOM.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
