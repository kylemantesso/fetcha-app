import React, { useContext } from "react";

import { Route, Switch, useRouteMatch } from "react-router-dom";
import { Checkout } from "../../screens/Checkout/Checkout";
import { Item } from "../../screens/Item/Item";
import { Menu } from "../../screens/Menu/Menu";
import { Payment } from "../../screens/Payment/Payment";
import { Receipt } from "../../screens/Receipt/Receipt";
import { ThemeContext } from "grommet";
import { MenuContext } from "../../context/MenuContext";

export const Venue = () => {
  const { path } = useRouteMatch();
  const { venue } = useContext(MenuContext);

  let theme = {};
  if (venue?.theme) {
    theme = {
      global: {
        colors: {
          brand: venue.theme.brand,
          "accent-1": venue.theme["accent-1"],
          focus: venue.theme["accent-1"],
        },
      },
    };
  }
  return (
    <ThemeContext.Extend value={theme}>
      <Switch>
        <Route exact path={`${path}/checkout`}>
          <Checkout />
        </Route>
        <Route path={`${path}/checkout/receipt/:amount`}>
          <Receipt />
        </Route>
        <Route
          path={`${path}/payment/:orderId/:merchantId/:locationId/:amount`}
        >
          <Payment />
        </Route>
        <Route exact path={[`${path}/`, `${path}/:categorySlug`]}>
          <Menu />
        </Route>
        <Route path={`${path}/:categorySlug/:itemSlug`}>
          <Item />
        </Route>
      </Switch>
    </ThemeContext.Extend>
  );
};
