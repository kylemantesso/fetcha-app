import { Route, Redirect } from "react-router-dom";
import { useAuthState } from "react-firebase-hooks/auth";
import firebase from "firebase";
import { RouteProps } from "react-router";
import React from "react";
import { Spinner } from "../../components/Spinner/Spinner";

export const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const [authUser, loading, error] = useAuthState(firebase.auth());
  return (
    <>
      {loading ? (
        <Spinner />
      ) : (
        <Route {...rest}>
          {authUser === null ? <Redirect to="/merchant/signup" /> : <>{React.cloneElement(children as any, { authUser })}</>}
        </Route>
      )}
    </>
  );
};
