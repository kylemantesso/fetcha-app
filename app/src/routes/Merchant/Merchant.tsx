import React from "react";

import { Route, Switch, useRouteMatch } from "react-router-dom";
import { SignUp } from "../../screens/SignUp/SignUp";
import { Dashboard } from "../../screens/Dashboard/Dashboard";
import { PrivateRoute } from "../PrivateRoute/PrivateRoute";
import { Setup } from "../../screens/Setup/Setup";
import { Link } from "../../screens/Link/Link";

export const Merchant = () => {
  const { path } = useRouteMatch();
  return (
      <Switch>
        <PrivateRoute path={`${path}/link`}>
          <Link />
        </PrivateRoute>
        <PrivateRoute path={`${path}/dashboard`}>
          <Dashboard />
        </PrivateRoute>
        <PrivateRoute path={`${path}/setup/:merchantId`}>
          <Setup />
        </PrivateRoute>
        <Route path={[`${path}/signup`, "/"]}>
          <SignUp />
        </Route>
      </Switch>
  );
};
