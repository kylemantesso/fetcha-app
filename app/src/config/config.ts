interface IConfig {
  firebase: {
    FUNCTION_URL: string;
    HOSTING_URL: string;
  };
  square: {
    APPLICATION_ID: string;
    AUTHORIZE_URL: string;
  };
}

export const ENV = process.env.NODE_ENV;
console.log('Environment: ' + ENV);

let config: IConfig;

if (ENV === "production") {
  config = {
    firebase: {
      FUNCTION_URL: "https://us-central1-bar-mate-au.cloudfunctions.net",
      HOSTING_URL: "https://fetcha.app",
    },
    square: {
      APPLICATION_ID: "sandbox-sq0idb-kVp0dZupqXoxLrNugT8Y1Q",
      AUTHORIZE_URL:
        "https://connect.squareupsandbox.com/oauth2/authorize?client_id=sandbox-sq0idb-kVp0dZupqXoxLrNugT8Y1Q&scope=ITEMS_READ MERCHANT_PROFILE_READ MERCHANT_PROFILE_READ ORDERS_WRITE PAYMENTS_WRITE",
    },
  };
} else if (ENV === "test") {
  config = {
    firebase: {
      FUNCTION_URL: "https://us-central1-bar-mate-au.cloudfunctions.net",
      HOSTING_URL: "https://fetcha.app",
    },
    square: {
      APPLICATION_ID: "sandbox-sq0idb-kVp0dZupqXoxLrNugT8Y1Q",
      AUTHORIZE_URL:
        "https://connect.squareupsandbox.com/oauth2/authorize?client_id=sandbox-sq0idb-kVp0dZupqXoxLrNugT8Y1Q&scope=ITEMS_READ MERCHANT_PROFILE_READ MERCHANT_PROFILE_READ ORDERS_WRITE PAYMENTS_WRITE",
    },
  };
} else {
  config = {
    firebase: {
      FUNCTION_URL: "http://localhost:5001/bar-mate-au/us-central1",
      HOSTING_URL: "https://localhost:3000",
    },
    square: {
      APPLICATION_ID: "sandbox-sq0idb-kVp0dZupqXoxLrNugT8Y1Q",
      AUTHORIZE_URL:
        "https://connect.squareupsandbox.com/oauth2/authorize?client_id=sandbox-sq0idb-kVp0dZupqXoxLrNugT8Y1Q&scope=ITEMS_READ MERCHANT_PROFILE_READ MERCHANT_PROFILE_READ ORDERS_WRITE PAYMENTS_WRITE",
    },
  };
}

export { config };
