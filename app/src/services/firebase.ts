import { request } from "./request";
import { config } from "../config/config";
import { ILocation } from "../../../shared/location";
import { IMenu } from "../../../shared/menu";
import { ICartRequest } from "../../../shared/cart";
import {IPaymentRequest} from "../../../shared/payment";

const BASE_URL = config.firebase.FUNCTION_URL;

const getMenuPath = (merchantId: string) => `${BASE_URL}/menu/${merchantId}`;

const getOrderPath = (merchantId: string, locationId: string) =>
  `${BASE_URL}/order/${merchantId}/${locationId}`;

const getPaymentPath = () =>
  `${BASE_URL}/payment`;

const getLocationsPath = (merchantId: string) =>
  `${BASE_URL}/locations/${merchantId}`;

export const getVenueReference = (venueId: string) =>
  `venues/${venueId}/public`;

export const getUserReference = (uid: string) => `users/${uid}`;

export const getTableReference = (venueId: string) =>
  `venues/${venueId}/public/tables`;

export const fetchMenu = (venueId: string): Promise<IMenu | undefined> =>
  request<IMenu>(getMenuPath(venueId));

export const fetchLocations = (
  merchantId: string
): Promise<ILocation[] | undefined> =>
  request<ILocation[]>(getLocationsPath(merchantId));

export const fetchPayment = (paymentRequest: IPaymentRequest) =>
  request<any>(getPaymentPath(), {
    method: "POST",
    body: JSON.stringify(paymentRequest),
  });


export const fetchOrder = ({
  merchantId,
  locationId,
  cartRequest,
}: {
  merchantId: string;
  locationId: string;
  cartRequest: ICartRequest;
}) =>
  request<any>(getOrderPath(merchantId, locationId), {
    method: "POST",
    body: JSON.stringify(cartRequest),
  });
