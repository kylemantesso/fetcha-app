export const request = <T = any>(
  input: RequestInfo,
  init?: RequestInit
): Promise<T | undefined> =>
  fetch(input, init)
    .then((res) => res.json())
    .catch((e) => console.error(e));
