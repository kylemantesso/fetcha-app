# Fetcha #

Fetcha - Just sit, we'll fetch!


## Features ##



Digital menu and checkout

Menu            |  Item | Checkout
:-------------------------:|:-------------------------:|:-------------------------:
![Menu](docs/menu.png)  |  ![Item](docs/items.png) |  ![Checkout](docs/checkout.png)

Online payment

Payment   | Thanks
:-------------------------:|:-------------------------:
![Payment](docs/payment.png)  |  ![Thanks](docs/thanks.png)



Orders integration

Orders |
:-----------------------------:|
![Orders](docs/orders.png)|



Merchant Dashboard and OAuth integration

Sign Up  |  Link Account | Dashboard
:-------------------------:|:-------------------------:|:-------------------------:
![Sign up](docs/signup.png)  |  ![Link](docs/link.png) |  ![Dashboard](docs/dashboard.png)

Landing page

Landing page |
:-----------------------------:|
![Orders](docs/landing.png)|



## /app ##

React client app

```
yarn install
yarn start
```

## /functions ##

Firebase functions backend
```
yarn install
yarn serve
```
